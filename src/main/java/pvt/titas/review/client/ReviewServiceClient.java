package pvt.titas.review.client;

import com.bazaarvoice.ostrich.ServiceEndPoint;
import pvt.titas.review.model.Review;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class ReviewServiceClient implements ReviewService {

   // private final Http _http;
    private final Client _client;
    private final UriBuilder _service;

    public ReviewServiceClient(ServiceEndPoint endPoint, Client jerseyClient) {
        this(Payload.valueOf(endPoint.getPayload()).getServiceUrl(), jerseyClient);
    }
    public ReviewServiceClient(URI endPoint, Client jerseyClient) {
        _client = checkNotNull(jerseyClient, "jerseyClient");
        _service = UriBuilder.fromUri(endPoint);
    }

    public List<Review> getAllReview() {
        return  _client
                .target(_service)
                .request()
                .get(Response.class)
                .readEntity(new GenericType<List<Review>>() {});
    }
    public Review getReviewById(Integer id){
        Response res =  _client
                .target(_service)
                .path(String.valueOf(id))
                .request()
                .get();
        return res.readEntity(Review.class);
    }
//    public Review addReview(Review review){
//        return _client
//                .target(_service)
//                .request()
//                .post();
//    }
}
