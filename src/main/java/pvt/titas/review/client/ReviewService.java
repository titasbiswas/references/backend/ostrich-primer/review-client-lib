package pvt.titas.review.client;

import pvt.titas.review.model.Review;

import java.util.List;

public interface ReviewService {
    List<Review> getAllReview();

    Review getReviewById(Integer id);

    //Review addReview(Review review);
}
